package index

import (
	"bytes"
	"fmt"
	"io"
	"justfts/internal/justfts/document"
	"justfts/internal/justfts/search"
	"os"
	"strconv"
	"strings"
)

const blockSize = 4096

type Index map[string][]int

func (idx Index) Add(docs []document.Document) {
	for _, doc := range docs {
		for _, token := range search.Analyze(doc.Text) {
			ids := idx[token]
			if len(ids) > 0 && contains(doc.ID, ids) {
				continue
			}

			idx[token] = append(ids, doc.ID)
		}
	}
}

func (idx Index) Search(text string) []int {
	var r []int
	for _, token := range search.Analyze(text) {
		if ids, ok := idx[token]; ok {
			if len(r) <= 0 {
				r = ids
			} else {
				r = append(r, ids...)
			}
		} else {
			return nil
		}
	}

	var list []int

	for _, item := range r {
		list = append(list, item)
	}

	return list
}

func (idx Index) Save(path string) error {
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer f.Close()

	for key, val := range idx {
		data := implode(",", val)
		msg := fmt.Sprintf("%d,%d:%s%s\n", len(key), len(data), key, data)

		err = blockWrite(msg, f)
		if err != nil {
			return err
		}
	}

	return nil
}

func (idx Index) Restore(path string) error {
	f, err := os.OpenFile(path, os.O_RDONLY, 0666)
	if err != nil {
		return err
	}
	defer f.Close()

	var offset int64 = 0

	var block []byte
	for ; ; {
		buff := make([]byte, blockSize)
		n, err := f.ReadAt(buff, offset)
		offset += int64(n)
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		block = append(block, buff...)
	}

	//scanner := bufio.NewScanner(f)
	//
	//for scanner.Scan() {
	//	pack := strings.Split(scanner.Text(), ":")
	//	if len(pack) < 2 {
	//		continue
	//	}
	//	body := strings.Split(pack[1], "->")
	//	key, value := body[0], body[1]
	//	value = value[1:]
	//	value = value[:len(value)-1]
	//
	//	idx[key] = explode(",", value)
	//}

	return nil
}
//
//func blockRead(r io.Reader, w io.Writer) error {
//
//	for ; ; {
//		buff := make([]byte, blockSize)
//		_, err := r.Read(buff)
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			return err
//		}
//
//		data := bytes.Split(buff, []byte(":"))
//		header := bytes.Split(data[0], []byte(","))
//		key, body := header[0], header[1]
//		keySize, err := strconv.Atoi(string(key))
//		if err != nil {
//			return err
//		}
//		bodySize, err := strconv.Atoi(string(body))
//		if err != nil {
//			return err
//		}
//
//
//	}
//
//	return blockWrite(msg[:blockSize], w)
//}

func blockWrite(msg string, w io.Writer) error {

	if len(msg) <= blockSize {

		block := bytes.NewBuffer(make([]byte, blockSize))
		block.Write([]byte(msg))

		_, err := io.Copy(w, block)
		if err != nil {
			return err
		}

		return nil
	}

	return blockWrite(msg[:blockSize], w)
}

func contains(item int, items []int) bool {
	for _, val := range items {
		if item == val {
			return true
		}
	}

	return false
}

func implode(sep string, items []int) string {
	data := ""
	size := len(items) - 1
	for id, item := range items {
		if id != size {
			data += fmt.Sprintf("%d%s", item, sep)
			continue
		}
		data = fmt.Sprintf("%d", item)
	}

	return data
}

func explode(sep string, text string) []int {
	var items []int

	for _, chunk := range strings.Split(text, sep) {
		item, _ := strconv.Atoi(chunk)
		items = append(items, item)
	}

	return items
}
