package search

import (
	"justfts/internal/justfts/document"
	"strings"
)

func Search(docs []document.Document, term string) []document.Document {
	var r []document.Document

	for _, doc := range docs {
		if strings.Contains(doc.Text, term) {
			r = append(r, doc)
		}
	}

	return r
}

