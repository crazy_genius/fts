package search

import (
	snowballeng "github.com/kljensen/snowball/english"
	"strings"
)

var stopWords = map[string]struct{}{ // I wish Go had built-in sets.
	"a": {}, "and": {}, "be": {}, "have": {}, "i": {},
	"in": {}, "of": {}, "that": {}, "the": {}, "to": {},
	"is": {},
}

type FilterFunc func(tokens []string) []string

func LowercaseFilter(tokens []string) []string {
	r := make([]string, 0, len(tokens))

	for _, token := range tokens {
		r = append(r, strings.ToLower(token))
	}

	return r
}

func StopWordFilter(tokens []string) []string {
	r := make([]string, 0, len(tokens))

	for _, token := range tokens {
		if _, ok := stopWords[token]; !ok {
			r = append(r, token)
		}
	}

	return r
}

func StemmerFilter(tokens []string) []string {
	r := make([]string, len(tokens))
	for i, token := range tokens {
		r[i] = snowballeng.Stem(token, false)
	}

	return r
}
