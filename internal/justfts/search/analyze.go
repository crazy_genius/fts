package search

func Analyze(text string) []string {
	filters := []FilterFunc{
		LowercaseFilter,
		StopWordFilter,
		StemmerFilter,
	}

	tokens := Tokenize(text)
	for _, filterFn := range filters {
		tokens = filterFn(tokens)
	}

	return tokens
}
