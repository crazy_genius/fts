package storage

import (
	"bytes"
	"encoding/binary"
	"io"
	"justfts/internal/justfts/index"
	"log"
	"os"
)

const fileTrailer = 0x50c0
const version = 1

type IndexFileHeader struct {
	FileTrailer uint32
	Version     uint32
	DataOffset  uint32
}

type IndexRowHeader struct {
	RowSize  uint32
	_        [1]byte
	KeySize  uint32
	DataSize uint32
}

type Storage struct {
	File *os.File
}

func NewStorage(f *os.File) Storage {
	return Storage{
		File: f,
	}
}

func (s Storage) Read(list index.Index) error {
	header := IndexFileHeader{}
	data, err := s.readNextBytes(12)
	if err != nil {
		return err
	}

	offset := 12
	buffer := bytes.NewBuffer(data)
	if err := binary.Read(buffer, binary.LittleEndian, &header); err != nil {
		log.Fatal("binary.Read failed", err)
	}

	if fileTrailer != header.FileTrailer {
		log.Fatalln("file not recognized")
	}

	for ; ; {
		rh := IndexRowHeader{}
		data, err = s.readNextBytes(13)
		offset += 13
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}

		buffer = bytes.NewBuffer(data)
		err = binary.Read(buffer, binary.LittleEndian, &rh)
		_, err = s.File.Seek(int64(offset), io.SeekStart)
		if err != nil {
			return err
		}

		data, err = s.readNextBytes(int(rh.KeySize))
		if err != nil {
			return err
		}
		offset += int(rh.KeySize)
		key := string(data)

		data, err = s.readNextBytes(int(rh.DataSize))
		if err != nil {
			return err
		}
		offset += int(rh.DataSize)
		count := int(rh.DataSize / 4)
		for i := 0; i < count; i++ {
			list[key] = append(list[key], int(binary.LittleEndian.Uint32(data[i*4:4+(i*4)])))
		}
	}

	return nil
}

func (s Storage) Save(index index.Index) error {
	header := IndexFileHeader{
		FileTrailer: fileTrailer,
		Version:     version,
		DataOffset:  12,
	}

	if err := binary.Write(s.File, binary.LittleEndian, header); err != nil {
		return err
	}

	for key, docs := range index {
		if err := s.putPart(key, docs); err != nil {
			return err
		}
	}

	return nil
}

func (s Storage) putPart(key string, docs []int) error {
	rh := IndexRowHeader{
		RowSize:  uint32(13 + len(key) + (len(docs) * 4)),
		KeySize:  uint32(len(key)),
		DataSize: uint32(len(docs) * 4),
	}

	if err := binary.Write(s.File, binary.LittleEndian, rh); err != nil {
		return err
	}
	if err := binary.Write(s.File, binary.LittleEndian, []byte(key)); err != nil {
		return err
	}

	for _, val := range docs {
		datum := make([]byte, 4)
		binary.LittleEndian.PutUint32(datum, uint32(val))
		if _, err := s.File.Write(datum); err != nil {
			return err
		}
	}

	return nil
}

func (s Storage) readNextBytes(size int) ([]byte, error) {
	buff := make([]byte, size)
	if _, err := s.File.Read(buff); err != nil {
		return nil, err
	}

	return buff, nil
}
