module justfts

go 1.14

require (
	github.com/RoaringBitmap/roaring v0.5.1
	github.com/kljensen/snowball v0.6.0
)
