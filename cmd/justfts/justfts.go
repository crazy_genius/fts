package main

import (
	"flag"
	"fmt"
	"io"
	"justfts/internal/justfts/document"
	"justfts/internal/justfts/index"
	"justfts/internal/justfts/storage"
	"log"
	"os"
	"path"
)

func main() {
	idx := make(index.Index)

	var rebuild bool

	flag.BoolVar(&rebuild, "rebuild", false, "rebuild index")
	flag.Parse()

	if rebuild {
		log.Println("Загрузка справочника ....")
		documents, err := document.LoadDocuments(path.Clean("./enwiki-latest-abstract1.xml"))

		if err != nil {
			os.Exit(255)
		}
		log.Println("Справочник загружен")

		log.Println("Построение индекса")
		idx.Add(documents)

		log.Println("Сохранение индекса")

		f, err := os.OpenFile("./data.fts", os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			log.Fatal(err)
		}

		s := storage.NewStorage(f)
		err = s.Save(idx)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Println("Загрузка индекса ....")
		f, err := os.OpenFile("./data.fts", os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			log.Fatal(err)
		}
		s := storage.NewStorage(f)
		err = s.Read(idx)
		if err != io.EOF && err != nil {
			log.Fatal(err)
		}
	}

	log.Println("Поиск по индексу")
	fmt.Println(idx.Search("cat"))

	log.Println("Выход")
}
